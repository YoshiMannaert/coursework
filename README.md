# Yoshi Mannaert 2IMDa 2014-2015 #

## Webtech2 ##

### 1 - version control with GIT ###

** clone**

You copy a repository to your local development system
this is usually done when setting up a project 

**add / commit**

Add: you add the file index.html to version control

Commit: commit the changes

**push / pull**

Pull: fetch+merge latest changes from a repository

Push: push your commits to the repository at alias origin and to the branch master

**branching / checkout**

Branches are used to create different parts of your project, without having to make changes to the main version of your project (aka the Master branch).

Checkout is used if you want to change which branch you are working in

**merge / rebase**

If you have finished creating a new feature of your project in a branch and you want to add this to the Master branch you will merge the two branches.

Rebase is an alternative to "merge" as it combines multiple branches.

**Centralized vs Distributed Version Control**

Centralized: Centralized version control systems are based on the idea that there is a single “central” copy of your project somewhere and programmers will “commit” their changes to this central copy.

Distributed: Distributed version control systems do not necessarily rely on a central server to store all the versions of a project’s files. Instead, every developer “clones” a copy of a repository and has the full history of the project on their own hard drive. This copy has all of the metadata of the original.

### 2 - mastering CSS animations ###

**Transitions:** A transition happens in between animations, it only starts once the object you want to animate gets a class added to it or once it has a pseudo-selector on it, for example once you hover over it.

**Transformations:** A transformation is an animation that changes how something physically looks. It can twist and spin and turn your object and it can animate it in 2D as well as in 3D. 

**Animations:** An animation uses keyframes and it can start automatically (doesn't have to be triggered) and it can loop.

You can make 3D transforms by using Perspective() to change the angle at which you look at your object. You can turn your object by using rotateX or rotateY. 

### 3 - Advanced Javascript ###

**Javascript:**  JavaScript is used to program the behavior of web pages. it's an interpreted, Object-Oriented Language. It is based on prototypes and isn't only useful in browsers but also outside of them. It's a necessity for any web-developer who wants to find a good job.

**Prototypes:** Prototypes are used so you don't have to write the same code twice. It's really flexible but you do have to use a wrapper object in case a DOM implementation is changed.

**chaining:** chaining is adding multiple functions to one object.

### 4 - Building an app prototype with APIs ###

Javascript uses functions instead of classes. constructors exist as well as function arguments. Adding functions to objects can
be done by adding … functions

prototype can be seen as the reference to the base class

**AJAX:** Asynchronous Javascript And XML, JSON can be used as well.

**Same-origin policy:** for security reasons scripts on one domain cannot interact with pages on another domain.

**JSONP:** JSON with padding, add a script tag to your page and let it execute a function wrapping incoming data. typically, a function name is used as a callback parameter 

**localstorage:** localstorage allows us to save data locally, while cookies also work but can contain much less data and are transmitted to the server with every request, we don’t want that overhead. localstorage is client-side only, cookies can store around 4kb while localstorage can store up to 5MB per domain.

### 5 - realtime apps with node.js and web sockets ### 

**NodeJS:** NodeJS is is server side Javascript, which allows you to do the same things you can do with PHP and are mainly used for apps where data is constantly updated. To be able to do this it is performance oriented. Everything has a callback which allows to let everything else work while one object is handling a request.(this is what we call Asynchronous)
It's modular and just like Javascript it's important to know this if you want to find a good job.

**Websockets:** Websockets allow for back and forth communication, there is a decent support for it however a server is needed and there needs to be fallbacks for those working with older browsers.


Working with Websockets:

**Package.json:** this is the description of your app and allows us to define the dependencies which you use for your app.

**Jade:** jade is a template language, it's like HTML but more abstract and easier to use

*Routes:** the routes folder is used to separate the concerns of executing code based on a specific route


### 6 - Angular.js ###

**Angular:** Angular is a  Javascript Framework.It works fully clientside and is useful for Single Page Apps. There is no need for dependencies and you can start using it even if you don't know any Javascript.

**Data Binding:** Data-binding is an automatic way of updating the view whenever the model changes, as well as updating the model whenever the view changes

**Controller:** Controllers are the behavior behind the DOM elements.

**Directives:** Directives let you invent new HTML syntax, specific to your application.

### 7 - SASS + BEM ###

**BEM:** Bem stands for " block, element, modif" and it is a methodology to structure and name your CSS, itgive your CSS more meaning and transparency to other developers and team members, it allows you to transfer CSS to different projects.

**block:** this is an object in your websites 

**element:** an element only makes sense within the contents of a block 

**modifier:** a modifier is a variation of a block

### 8 - GULPJS ###

**Gulp** is an task runner/build tool, which runs on Node.JS.

It allows you to merge and minify CSS files, compress images, remove unused CSS, Test, merge and compress
Javascript and convert SASS to CSS.