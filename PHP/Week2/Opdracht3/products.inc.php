<?php
	$arr_products = [

		[
			"name"    => "Bring me the Horizon - Sleepwalking",
			"price"   => 12.50,
			"picture" => "http://eil.com/images/main/Bring-Me-The-Horizon-Sleepwalking---Re-583356.jpg", 
		],
		[
			"name"    => "A Day to Remember - Homesick",
			"price"   => 24.99,
			"picture" => "http://images.victoryrecords.com/albums/VR448-LP-GREEN.jpg", 
		],
		[
			"name"    => "Chelsea grin - Ashes to ashes",
			"price"   => 20.00,
			"picture" => "http://static.underthegunreview.net/uploads/2014/05/Screen-Shot-2014-05-12-at-2.03.15-PM.png", 
		],
		[
			"name"    => "Nirvana - Bleach",
			"price"   => 29.99,
			"picture" => "http://eil.com/Gallery/458675b.jpg"
		],
		[
			"name"    => "Lorde - Pure Heroine",
			"price"   => 23.99,
			"picture" => "https://shop.jbhifi.co.nz/rkt/MEDIUM//45/77/4577113.jpg", 
		],
		[
			"name"    => "Marilyn Manson - Pale Emperor",
			"price"   => 25.99,
			"picture" => "http://images.visions.de/verlosungen/detail/2252.jpg", 
		]
	];
?>