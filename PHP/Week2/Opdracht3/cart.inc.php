<?php 
include_once 'products.inc.php';
if( !isset($_SESSION['cart']) ) {
       $_SESSION['cart']=array();
}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cart</title>
</head>
<body>
	<div id="cart">
	<h2>Products in your cart</h2>
	<p><i>There are <?php 
		echo count($_SESSION['cart']);
	?> items in your cart</i></p>
	<ul>
		<?php foreach ($_SESSION['cart'] as $p_id):?>
		<li>
			<?php echo $arr_products[$p_id]['name'] . " - &euro; " . number_format($arr_products[$p_id]['price'],2, ',', ' ') ?>
		</li>
		<?php endforeach ?>
	</ul>
	</div>
</body>
</html>