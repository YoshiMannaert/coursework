<?php 
	include("data.inc.php"); 
	$number_of_users = count($users);
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>
		
	</title>
	<link rel="stylesheet" href="css/ello.css">
</head>
<body class="homepage">

	<?php include("nav.inc.php"); ?>
	
	<div class="users_container">
		<?php if( isLoggedIn()): ?> 
	<ul class="users">
		
<?php
		if (isset($number_of_users)) {
		for ($i=0; $i < $number_of_users ; $i++) 
		{ 	
			echo "<a href='profile.php?id=" . $i . "'>";
			echo "<img src='" . $users[$i]['picture'] . "'></a>";
			
		}
	}
		?>

	</ul>
	<?php endif; ?>
	<br class="clearfix">
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script>
	$(document).ready(function(){

		$(".users_container").mousemove(function( event ) {
		  var x = Math.round(event.pageX/5) * -1;
		  var y = Math.round(event.pageY/5) * -1;
	  		$(".users").css("transform", "translateX("+x+"px) translateY("+y+"px)");
		});

		$("body").mousemove();

	});
	</script>

</body>
</html>