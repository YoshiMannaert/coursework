<?php
	include_once("../classes/Message.class.php");
	$m = new Message();
	
	//controleer of er een update wordt verzonden
	if(!empty($_POST))
	{
		$m->Text = $_POST['update'];
		try 
		{
			$m->Create();

			// PHP != JS, json_encode
			$arr_response = [
				"status" => "succes",
				"update" => $m->Text
			];
		} 
		catch (Exception $e) 
		{
			$feedback = $e->getMessage();

			$arr_response = [
				"status" => "error",
				"update" => $feedback
			];
		}
		header('Content-Type: application/json');
		echo json_encode($arr_response);
	}
?>