<?php
	$array = array(
				array("description" => "Design Website",
					  "deadline" => "2015-02-18 22:05:00",
					  "category" => "school"
				),
				array("description" => "Finish CMS",
					  "deadline" => "2015-02-18 23:05:00",
					  "category" => "work"
				),
				array("description" => "Take a Break",
					  "deadline" => "2015-02-18 23:55:00",
					  "category" => "work"
				),
				array("description" => "Make food",
					  "deadline" => "2015-02-18 23:25:00",
					  "category" => "home"
				),
				array("description" => "Finish PHP",
					  "deadline" => "2015-02-19 05:05:00",
					  "category" => "home"
				),
				array("description" => "Get rich",
					  "deadline" => "2015-02-19 23:05:00",
					  "category" => "home"
				),
				array("description" => "Die tryin'",
					  "deadline" => "2015-02-20 15:05:00",
					  "category" => "home"
				)
			);
?><!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>To Do</title>
	<STYLE TYPE="text/css">
		body{
			margin: 0px;
		}
		header
		{
			text-align: center;
			background-color: #303947;
			color: white;
			padding-top: 5px;
			padding-bottom: 5px;
			margin: 0;
		}
		h1{
			font-size: 16px;
		}
		.container{
			width: 320px;
			margin: 0 auto;
			font-family: Arial, sans-serif;
			font-size: 16px;
		}

		.list{
			list-style-type: disc;
			padding-left: 0px;
			margin: 0;
		}
		li{
			padding: 2px 2px;
			border-top: 1px solid rgba(0,0,0,0.2);
			text-align: left;
			padding-left: 20px;
			background-color: #323A4B;
			list-style-position: inside;
		}

		li.green{
			color: #22A51F;
		}

		li.orange{
			
			color: #EDA01E;
		}

		li.red{
			
			color: #E52715;
		}
		li.black{
			
			color: black;
		}
		
		ul p{
			padding-left: 15px;
			font-size: 12px;
			color: white;
		}
		ul h1
		{
			color: white;
		}

	</STYLE>
</head>
<body>
	<div class="container">
		<header><?php echo date('d-m-Y H:i:s'); ?></header>
		<?php


			echo "<ul class='list'>";

			 foreach ($array as $todo) {
			 	$then = $todo['deadline'];
				$current = date('d-m-Y H:i:s');

				$thenTimeStamp = strtotime($then);
				$currentTimestamp = strtotime($current);

				$difference = $thenTimeStamp - $currentTimestamp;
				$minutes = floor($difference / 60);

			 	if ($minutes <= "00")
			 	{
			 		$class = 'class="black"';
			 	}
			 	elseif ($minutes <= "120")
			 	{
			 		$class = 'class="red"';
			 	}
			 	elseif ($minutes <= "1440") {
			 		$class = 'class="orange"';
			 	}
			 	elseif ($minutes > "1440") {
			 		$class = 'class="green"';
			 	}

			 	echo "<li ".$class.">";
			 		echo "<h1>" . $todo['description'] . "</h1>";

				 	if ($todo['deadline'] == "1")
				 	{
				 		echo "<p>TIME LEFT: " . $minutes. " minutes</p>";
				 	}
				 	else
				 	{
				 		echo "<p>TIME LEFT: " . $minutes. " minutes</p>";
				 	}

			 	echo "</li>";
			 }

			


			echo "</ul>"
		?>
	</div>

</body>
</html>