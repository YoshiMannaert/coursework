<?php
function get_current($name) {
  if (strpos($_SERVER['REQUEST_URI'], $name) !== false)
    echo 'class="current"';
}
?><nav>
	<a <?php get_current('index') ?> href="index.php">home</a>
	<a <?php get_current('contact') ?> href="contact.php">contact</a>
</nav>