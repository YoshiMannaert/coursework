<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Yoshi Mannaert | Home</title>
	<link rel="stylesheet" href="normalize.css">
	<link rel="stylesheet" href="screen.css">
</head>
<body>
	
	<h1>Homepage</h1>
	<?php include_once("nav.inc.php"); ?>

	<!--Dynamic part of the theme -->
	<div id="content">
		<h2>This is the homepage</h2>
		<section class="container">
    <table class="cal">
      <caption>
        <span class="prev"><a href="#">&larr;</a></span>
        <span class="next"><a href="#">&rarr;</a></span>
        Febuary 2015
      </caption>
      <thead>
        <tr>
          <th>Mon</th>
          <th>Tue</th>
          <th>Wed</th>
          <th>Thu</th>
          <th>Fri</th>
          <th>Sat</th>
          <th>Sun</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="off"><a href="index.php">26</a></td>
          <td class="off"><a href="index.php">27</a></td>
          <td class="off"><a href="index.php">28</a></td>
          <td class="off"><a href="index.php">29</a></td>
          <td class="off"><a href="index.php">30</a></td>
          <td class="off"><a href="index.php">31</a></td>
          <td><a href="index.php">1</a></td>
        </tr>
        <tr>
          <td><a href="index.php">2</a></td>
          <td><a href="index.php">3</a></td>
          <td><a href="index.php">4</a></td>
          <td><a href="index.php">5</a></td>
          <td><a href="index.php">6</a></td>
          <td><a href="index.php">7</a></td>
          <td><a href="index.php">8</a></td>
        </tr>
        <tr>
          <td><a href="index.php">9</a></td>
          <td><a href="index.php">10</a></td>
          <td><a href="index.php">11</a></td>
          <td><a href="index.php">12</a></td>
          <td><a href="index.php">13</a></td>
          <td class="active"><a href="index.php">14</a></td>
          <td><a href="index.php">15</a></td>
        </tr>
        <tr>
          <td><a href="index.php">16</a></td>
          <td><a href="index.php">17</a></td>
          <td><a href="index.php">18</a></td>
          <td><a href="index.php">19</a></td>
          <td><a href="index.php">20</a></td>
          <td><a href="index.php">21</a></td>
          <td><a href="index.php">22</a></td>
        </tr>
        <tr>
          <td><a href="index.php">23</a></td>
          <td><a href="index.php">24</a></td>
          <td><a href="index.php">25</a></td>
          <td><a href="index.php">26</a></td>
          <td><a href="index.php">27</a></td>
          <td><a href="index.php">28</a></td>
          <td><a href="index.php">29</a></td>
        </tr>
        <tr>
          <td><a href="index.php">30</a></td>
          <td><a href="index.php">31</a></td>
          <td class="off"><a href="index.php">1</a></td>
          <td class="off"><a href="index.php">2</a></td>
          <td class="off"><a href="index.php">3</a></td>
          <td class="off"><a href="index.php">4</a></td>
          <td class="off"><a href="index.php">5</a></td>
        </tr>
      </tbody>
    </table>
  </section>
	</div>

	<?php include_once("footer.inc.php"); ?>

</body>
</html>