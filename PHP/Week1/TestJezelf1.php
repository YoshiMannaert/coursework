<?php 
	//result, normaal vanuit sql/db
	//$post = array();   oude versie
 	//vanaf php 5.4:
	$now = date('d-m-Y H:i:s');
	$post = [
				[
					"icon"     => "https://s3.amazonaws.com/uifaces/faces/twitter/pinceladasdaweb/128.jpg",
					"username" => "Theresa W.",
					"location" => "East River Park",
					"city"     => "Brooklyn, NY",
					"time"     => "2015-02-16 14:05:00",
					"picture"  => "http://cnjtc.com/wp-content/uploads/2013/05/afternoon-on-manhattan-city-imac-wallpaper.jpg"
					
				],
				[
					"icon"     => "https://s3.amazonaws.com/uifaces/faces/twitter/iconfinder/128.jpg",
					"username" => "Nina M.",
					"location" => "Rubirosa",
					"city"     => "New York, NY",
					"time"     => "2015-02-16 13:57:00",
					"picture"  => ""
					
				],
				[
					"icon"     => "",
					"username" => "Sean B.",
					"location" => "Blue Bottle Coffee",
					"city"     => "South of Market, San Francisco",
					"time"     => "2015-02-13 11:15:00",
					"picture"  => ""
					
				]
			];

		//auto refresh page
		$page = $_SERVER['PHP_SELF'];
		$sec = "60";

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Swarm</title>
	<!--auto refresh page-->
	<meta http-equiv="refresh" content="<?php echo $sec?>;URL='<?php echo $page?>'">
	<style type="text/css">
		body{
			text-align: center;
			background-color: #E9EAED;
			margin: 0;
			font-family: "Roboto", Arial, sans-serif;
			font-size: 13px;
		}
		header
		{
			background-color: #1DAEEC;
			color: white;
			width: 420px;
			margin-right: auto;
			margin-left: auto;
			padding-top: 5px;
			padding-bottom: 5px;
		}
		nav ul li 
		{
			list-style: none;
		}
		nav  
		{
			clear: both;
			margin-top: 0;
			margin-bottom: 0;
			margin-right: auto;
			margin-left: auto;
			width: 420px;
			padding: 0;
		}
		nav  a:link,
		nav  a:visited,
		nav  a:hover,
		nav  a:active
		{
			text-decoration: none;
			color: white;
			background-color: #1DAEEC;
			display: block;
			width: 210px;
			float: left;
			padding-top: 10px;
			padding-bottom: 10px;
		}
		nav a:hover
		{
			background-color: #188BB8;
		}
		.post
		{	
			text-align: left;
			clear: both;
			background-color: white;	
			width: 400px;
			margin-left: auto;
			margin-right: auto;
			border-bottom: 1px lightgrey solid;
			padding: 10px;
		}
		.post a:link,
		.post a:visited,
		.post a:hover,
		.post a:active
		{
			text-decoration: none;
		}
		#icon
		 {
		 	border-radius: 50%;
		 	max-width: 60px;
		 	float: left;
		 	margin-right: 20px;
		 	margin-bottom: 20px;
		 }
		 .post img
		 {
		 	max-width: 100%;
		 }
		 .post a 
		 {
		 	font-size: 16px;
		 }
		 .none
		 {
		 	display: none;
		 }
	</style>
</head>
<body>
	
	<header>
		<?php echo $now; ?>
	</header>
	<nav>
		<a  href="#">Old</a>
		<a  href="#">New</a>
	</nav>
	<?php foreach ($post as $p) { ?>
	<?php

				 if(empty($p['icon']))
				 {
				 	$p['icon'] = "http://common.bbcomcdn.com/images/default-avatar/Avatar-Unisex-Default_295x295.jpg";
				 };
		?>
	<div class="post">
		<img id="icon" src="
			<?php 
				echo $p['icon'];
			?>
		">
		<a href="#">
			<?php 
				echo $p['username'];
			?>
		</a>
		</br>
		<a id="location" href="#">
			<?php 
				echo $p['location'];
			?>
		</a>
		<p>
			<?php 
				echo $p['city'];
			?>
		</p>
		<img src="
			<?php 
				echo $p['picture'];
			?>"">
		<span>
			<?php 
				$then = $p['time'];
				$current = date('d-m-Y H:i:s');

				$thenTimeStamp = strtotime($then);
				$currentTimestamp = strtotime($current);

				$difference = $currentTimestamp - $thenTimeStamp;
				$minutes = floor($difference / 60);
				
				if($minutes<=60)
				{
					echo "<span class='tijd'>" . $minutes . "m</span>";
				}
				else if($minutes<=120)
				{
					echo "<span class='tijd'>1h" . ($minutes-60) . "m</span>";
				}
				else if($minutes<=180)
				{
					echo "<span class='tijd'>2h" . ($minutes-120) . "m</span>";
				}
				else
				{
					echo "<span class='tijd'>3h+</span>";
				}

				?>
		</span>
	</div>
	<?php } ?>
</body>
</html>