<?php  
    include_once("Voertuig.class.php");

    class Vrachtwagen extends Voertuig
    {
    	private $m_iMaxLast;
    	
        public function __set($p_sProperty, $p_vValue)
        {
            parent::__set($p_sProperty, $p_vValue);

            switch ($p_sProperty) 
            {
                case 'MaxLast':
                    $this->m_iMaxLast = $p_vValue;
                    break;
            }
        }

        public function __get($p_sProperty)
        {
            $vResult = parent::__get($p_sProperty);
            switch ($p_sProperty) 
            {
                case 'MaxLast':
                    $vResult =  $this->m_iMaxLast;
                    break;
            }
            return $vResult;
        }

        public function Save()
        {
            if (date('H') > 12){
                throw new Exception("U kan maar reserveren tot 12u!");
            } else {
                $conn = new PDO('mysql:host=localhost; dbname=phpopdracht2', 'root', '');

                $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //2 - query, watch out for SQL injection ( ' " )
                $statement = $conn->prepare("INSERT INTO vrachtwagens ( merk, aantal_passagiers, aantal_deuren, max_last) 
                    VALUES (:Merk, :AantalPassagiers, :AantalDeuren, :MaxLast)"); 
                $statement->bindValue(":Merk", $this->Merk);
                $statement->bindValue(":AantalPassagiers", $this->AantalPassagiers);
                $statement->bindValue(":AantalDeuren", $this->AantalDeuren);
                $statement->bindValue(":MaxLast", $this->MaxLast);
                $statement->execute();
                
            }
        }
        public function Reserveer()
        {
            $conn = new PDO('mysql:host=localhost; dbname=phpopdracht2', 'root', '');
           $result = $conn->query("SELECT * FROM vrachtwagens;");
            return $result; 
        }
    }
?>