<?php 
	include_once ('iMachine.class.php');
	abstract class Voertuig implements iMachine
	{
		private $m_sMerk;
    	private $m_iAantalPassagiers;
    	private $m_iAantalDeuren;

    	public function __set( $p_sProperty, $p_vValue )
	    {
	    	switch ($p_sProperty) {
	    		case 'Merk':
	    		if ($p_vValue !== ''){
						$this->m_sMerk = $p_vValue;
					} else {
						throw new Exception('Vul een Merk in!');
					}
	    			
	    			break;

	    		case 'AantalPassagiers':
	    			$this->m_iAantalPassagiers = $p_vValue;
	    			break;

	    		case 'AantalDeuren':
	    			$this->m_iAantalDeuren = $p_vValue;
	    			break;
	    		
	    	}
	    }

	    public function __get( $p_sProperty )
	    {
	    	switch ($p_sProperty) {
	    		case 'Merk':
	    			return $this->m_sMerk;
	    			break;
	    		
	    		case 'AantalPassagiers':
	    			return $this->m_iAantalPassagiers;
	    			break;

	    		case 'AantalDeuren':
	    			return $this->m_iAantalDeuren;
	    			break;
	    	}
	    }

	    /*public function Reserveer()
        {
            //this function saves the object to the database
            //1 - make connection PDO vs mysqli
            $conn = new PDO('mysql:host=localhost; dbname=phpopdracht2', 'root', '');
            //show error message when it doesn't work
            $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //2 - query, watch out for SQL injection ( ' " )
            $statement = $conn->prepare("INSERT INTO auto ( merk, AantalPassagiers, AantalDeuren) VALUES (:merk, :AantalPassagiers, :AantalDeuren)"); 
            $statement->bindValue(":merk", $this->merk);
            $statement->bindValue(":AantalPassagiers", $this->AantalPassagiers);
            $statement->bindValue(":AantalDeuren", $this->AantalDeuren);
            $statement->execute();
        }*/








	}
?>