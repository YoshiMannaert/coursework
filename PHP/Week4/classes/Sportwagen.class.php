<?php  
    include_once("Voertuig.class.php");

    class Sportwagen extends Voertuig
    {
    	private $m_bStereoInstallatie;
    	
        public function __set($p_sProperty, $p_vValue)
        {
            parent::__set($p_sProperty, $p_vValue);

            switch ($p_sProperty) 
            {
                case 'Stereo':
                    $this->m_bStereoInstallatie = $p_vValue;
                    break;
            }
        }

        public function __get($p_sProperty)
        {
            $vResult = parent::__get($p_sProperty);
            switch ($p_sProperty) 
            {
                case 'Stereo':
                    $vResult =  $this->m_bStereoInstallatie;
                    break;
            }
            return $vResult;
        }

        public function Save()
        {
            if (date('H') > 12){
                throw new Exception("U kan maar reserveren tot 12u!!");
            } else {
                $conn = new PDO('mysql:host=localhost; dbname=phpopdracht2', 'root', '');

                $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                //2 - query, watch out for SQL injection ( ' " )
                $statement = $conn->prepare("INSERT INTO sportwagens ( merk, aantal_passagiers, aantal_deuren, stereo_installatie) 
                    VALUES (:Merk, :AantalPassagiers, :AantalDeuren, :Stereo)"); 
                $statement->bindValue(":Merk", $this->Merk);
                $statement->bindValue(":AantalPassagiers", $this->AantalPassagiers);
                $statement->bindValue(":AantalDeuren", $this->AantalDeuren);
                $statement->bindValue(":Stereo", $this->Stereo);
                $statement->execute();
                
                
            }
        }
        public function Reserveer()
        {
            $conn = new PDO('mysql:host=localhost; dbname=phpopdracht2', 'root', '');
           $result = $conn->query("SELECT * FROM sportwagens;");
            return $result; 
        }

    }
?>