<?php
spl_autoload_register( function($class)
	{
		include_once("classes/" . $class . ".class.php");
	} );

$sport = new Sportwagen();
$vracht = new Vrachtwagen();
if (isset($_POST['sportwgn'])) {
	if (!empty($_POST)) 
{
	try 
	{
	   $sport->Merk = $_POST['merk'];
	   $sport->AantalPassagiers = $_POST['AantalPassagiers'];
	   $sport->AantalDeuren = $_POST['AantalDeuren'];
	   $sport->Stereo = isset($_POST['stereo']) ? true : false;
	   $sport->Save();
	   $succes= "Uw sportwagen is gereserveerd!";
	}
	catch( Exception $e)
	{
		$error = $e->getMessage();
	}
}
}
if (isset($_POST['vrachtwgn'])) {
if (!empty($_POST)) 
{
	try 
	{
	   $vracht->Merk = $_POST['merk'];
	   $vracht->AantalPassagiers = $_POST['AantalPassagiers'];
	   $vracht->AantalDeuren = $_POST['AantalDeuren'];
	   $vracht->MaxLast = $_POST['MaxLast'];
	   $vracht->Save();
	   $succes= "Uw vrachtwagen is gereserveerd!";
	}
	catch( Exception $e)
	{
		$error = $e->getMessage();
	}
}
}

$sportwagens = $sport->Reserveer();
$vrachtwagens = $vracht->Reserveer();

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PHP OOP</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="screen.css">
	<link rel="stylesheet" href="animate.css">
</head>
<body>
	<?php if (isset($error)): ?>
	<div class="error animated wobble">
		<?php  echo $error ?>
	</div>
<?php  endif; ?>
	<?php if (isset($succes)): ?>
	<div class="succes animated bounceInDown"><?php echo $succes; ?></div>
<?php  endif; ?>
<div id="centered">
<div id="container">
<h1 class="box">Reserveer een Sportwagen!</h1>
<div id="content">
	
	<form action="" method="post">
		<label for="merk">merk</label>
		<input type="text" id="merk" name="merk">
		<br>
		<label for="AantalPassagiers">Aantal passagiers</label>
		<input type="number" min="2" max="6" id="AantalPassagiers" name="AantalPassagiers">
		<br>
		<label for="AantalDeuren">Aantal deuren</label>
		<input type="number" min="2" max="5" id="AantalDeuren" name="AantalDeuren">
		<br>
		<label for="stereo">Stereo?</label>
		<input type="checkbox" name="stereo" id="stereo" value="stereo"><br>
		<br></div><div class="box">
		<button type="submit" name="sportwgn">Reserveer</button></div>
	</form>

</div>
</div>
<div id="container">
<h1 class="box">Reserveer een Vrachtwagen!</h1>
<div id="content">
	
	<form action="" method="post">
		<label for="merk">merk</label>
		<input type="text" id="merk" name="merk">
		<br>
		<label for="AantalPassagiers">Aantal passagiers</label>
		<input type="number" min="1" max="6" id="AantalPassagiers" name="AantalPassagiers">
		<br>
		<label for="AantalDeuren">Aantal deuren</label>
		<input type="number" min="2" max="5" id="AantalDeuren" name="AantalDeuren">
		<br>
		<label for="MaxLast">Max last</label>
		<input type="number" min="1" max="5" id="MaxLast" name="MaxLast"><br>
		<br></div><div class="box">
		<button type="submit" name="vrachtwgn">Reserveer</button></div>
	</form>

</div>
</div>
</div>

<div id="reservation" class="animated fadeIn">
	<?php if (isset($succes)): ?>
		<?php 
			echo "<h3>U hebt een " . $_POST['merk'] . " geboekt.</h3><br>";
			echo "U kunt hier met " . $_POST['AantalPassagiers'] . " mensen in zitten. <br>";
			echo "De auto heeft  " . $_POST['AantalDeuren'] . 
			" deuren<br>";
			if (isset($_POST['sportwgn'])) {
				if (isset($_POST['stereo']) == true) {
					echo "De auto heeft een sereo installatie<br><br>";
				}
				else
				{
					echo "De auto heeft geen sereo installatie<br><br>";
				}
				
			}
			if (isset($_POST['vrachtwgn']))
			{
				echo "De vrachtwagen heeft een Maximum last van " . $_POST['MaxLast'] . " ton<br><br>";
			}
		

		?>
	<?php  endif; ?>

</div>
</body>
</html>