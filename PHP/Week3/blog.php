<?php 
$conn = new PDO('mysql:host=localhost; dbname=phpopdracht1', 'root', '');
$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//$conn = new mysqli("localhost", "root", "", "phpopdracht1");
session_start();
	if (!empty($_POST)) 
	{
			$title = $_POST['title'];
			$article = $_POST['article'];
			$username = $_SESSION['loggedIn']['username'];
			$time = date("y-m-d");
			
			//$sql = "INSERT INTO blog (title, article, username) VALUES ('" . $conn->real_escape_string($title)."', '".$conn->real_escape_string($article). "', '" . $_SESSION['loggedIn']['username'] . "')";
			
			$statement = $conn->prepare("INSERT INTO blog (title, article, username, time) VALUES (:title, :article, :username, :time)"); 
            $statement->bindValue(":title", $title);
            $statement->bindValue(":article", $article);
            $statement->bindValue(":username", $username);
            $statement->bindValue(":time", $time);
            $statement->execute();

			//$conn->query($sql);
	}	
	//$sql = "SELECT * FROM blog";
	//$blogposts = $conn->query($sql);
	//var_dump($_SESSION);

	if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] != true) {
    	header("location:login.php");
}

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Blog</title>
	<link rel="stylesheet" href="screen.css">
</head>
<body>
	<header></header>
	<h2>Tell us what you're up to, <?php echo $_SESSION['loggedIn']['username'] ?>!</h2>
	<form action="" method="post">
		<input type="text" name="title" id="title" placeholder="Title">
		<br>
		<br>
		<textarea name="article" placeholder="Type here" id="" cols="70" rows="10"></textarea>
		<br>
		<br>
		<button type="submit" id="add">+</button>
		<br>
		<hr>
	</form>
	<a href="posts.php">See what your friends have been up to!</a>
	<a href="Logout.php">click here to log out</a>
</body>
</html>