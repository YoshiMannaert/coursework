<?php 
	session_start();
	if (!empty($_POST)) 
	{

		$options = [
    	'cost' => 12,
		];

		$username = $_POST['username'];
		$email = $_POST['email'];
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$password = password_hash($_POST['password'], PASSWORD_DEFAULT, $options);

		if (!empty($username) && !empty($email) && !empty($firstname) && !empty($lastname) && !empty($password)) 
		{		
		$conn = new PDO('mysql:host=localhost; dbname=phpopdracht1', 'root', '');
		$conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//$conn = new mysqli("localhost", "root", "", "phpopdracht1");
		//check if connection to server works (password either "" (empty) or "root")
			$statement = $conn->prepare("INSERT INTO users (username, password, email, firstname, lastname) VALUES (:username, :password, :email, :firstname, :lastname)"); 
            $statement->bindValue(":username", $username);
            $statement->bindValue(":password", $password);
            $statement->bindValue(":email", $email);
            $statement->bindValue(":firstname", $firstname);
            $statement->bindValue(":lastname", $lastname);
            //$statement->execute();
            $status = $statement->execute();
			//echo "connection ok";
			//$query = "INSERT INTO users (username, password, email, firstname, lastname) VALUES ('".$conn->real_escape_string($username)."', '".$conn->real_escape_string($password)."', '".$conn->real_escape_string($email)."', '".$conn->real_escape_string($firstname)."', '".$conn->real_escape_string($lastname)."')";
			if( $status )
			{
				header('Location: blog.php');
			}
		}
		else {
			$feedback = "please fill in everything";
		}
	}
	if (empty($_POST)) 
	{
		$feedback = "please fill in the form";
	}

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Signup</title>
	<link rel="stylesheet" href="screen.css">
</head>
<body>
	<header></header>
	
	<?php 
		if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] == true): ?>
	<h1>You are already logged in</h1>
	<a href='Logout.php'>click here to log out</a>
<?php else: ?>
	<h4><?php
	if (isset($feedback)) {
		echo $feedback;
	}
	  
	 ?></h4>
	<h1>Sign up!</h1>
	<form action="" method="post" id="register">
		<div id="mid">
			<label for="username">Username</label><br>
			<input type="text" id="username" name="username" />
			<br><br>
			<label for="password">Password</label><br>
			<input type="password" id="password" name="password" />
			<br><br>
			<label for="email">email</label><br>
			<input type="text" id="email" name="email" />
			<br><br>
			<label for="firstname">firstname</label><br>
			<input type="text" id="firstname" name="firstname" />
			<br><br>
			<label for="lastname">lastname</label><br>
			<input type="text" id="lastname" name="lastname" />
			<br><br>
			<button type="submit">Create my account</button>
	<a href="login.php">Or log in!</a>
		</div>
	</form>
<?php endif; ?>
</body>
</html>