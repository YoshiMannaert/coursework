<?php 
  $conn = new PDO('mysql:host=localhost; dbname=phpopdracht1', 'root', '');
  //$conn = new mysqli("localhost", "root", "", "phpopdracht1");


$statement = $conn->prepare("SELECT * FROM blog"); 
            $statement->execute();

	//$sql = "SELECT * FROM blog";
	//$blogposts = $conn->query($sql);

?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="screen.css">
</head>
<body>
	<header></header>
	<h2>Latest posts!</h2>
	<a href="blog.php">Post something</a>
	<a href="Logout.php">click here to log out</a>
	<?php 
		while ( $post = $statement-> fetchObject() ) 
		{
			// < -> &lt;
			echo "<div class='content'>";
			echo "<h1>" . htmlspecialchars($post->title) . "</h1>";
			echo "<p>" . htmlspecialchars($post->article) . "</p>";
			echo "<p> Posted by: " . htmlspecialchars($post->username) . "</p>";
			echo "<p> Posted on: " . htmlspecialchars($post->time) . "</p>";
			echo "</div>";
		}
	?>
</body>
</html>