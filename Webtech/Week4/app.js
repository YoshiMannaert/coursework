function bla() {
    /*global $, JQuery*/
    "use strict";
    function success(pos) {
        var lat = pos.coords.latitude;
        var long = pos.coords.longitude;
        var key = "6d8dffaf4dc89893ca569a92bdec821e";
        var url = "https://api.forecast.io/forecast/" + key + "/" + lat + "," + long;
        console.log(lat);
        console.log(url);
        console.log(long);
        
        var App, myApp;
        App = function () {
            this.showTemp = function () {
                
                var timeNow = new Date().getTime();
                var timeOut = localStorage.getItem("time");
                
                if (localStorage.getItem("current") !== null && timeNow < timeOut) {
                    var current = JSON.parse(localStorage.getItem('current'));
                } else {
                    //Get from SERVER!
                    localStorage.removeItem("current");
                    localStorage.removeItem("time");
                    var timeOut = new Date().getTime()+3600000;
                
                    var url = "https://api.forecast.io/forecast/" + key + "/" + lat + "," + long;
                    $.ajax({
                        type: 'GET',
                        dataType: 'jsonp',
                        url: url,
                        success: function (resp ) {
                            var current = resp.currently;
                            
                            localStorage.setItem("current", JSON.stringify(current));
                            localStorage.setItem("time", timeOut);
                            console.log("works");
                
                        },
                        error: function(){
                            console.log("fail");
                        }
                    });
                }
            
                $.each(current, function(key, currently){
                    console.log(current);
                    document.getElementById("temperature").innerHTML = Math.round((current.temperature-32)*5/9) + "°C";
                    document.getElementById("icon").src = "img/" + current.icon + ".svg";
                    document.getElementById("windSpeed").innerHTML = current.windSpeed + "m/s";
                    document.getElementById("precipProbability").innerHTML = current.precipProbability + "%";
                });
            };
    
        };
        myApp = new App();
        myApp.showTemp();
    };
    
    function error(err) {
        console.log("fails"); 
        
    }
    
    navigator.geolocation.getCurrentPosition(success, error);
}
bla();