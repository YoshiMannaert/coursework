var express = require('express');
var router = express.Router();

/* /asks/ */
router.get('/', function(req, res, next) {
  res.render('asks/index');
});

module.exports = router;